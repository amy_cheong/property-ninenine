> **Note**: The challenge consist of 3 parts.

Part 1: You are required to write a network wrapper for GET and POST call.

1. The required protocols is provided. You are required to comform to the protocol when you create the network wrapper.
2. You are only allowed to use URLSession to complete your wrapper.

>GetWeatherRequest and WeatherViewModel should be able to give you an idea how is the wrapper is going to be used.

Part 2: You are required to write unit test for the network wrapper that you have completed.

1. The unit test will need to include test case for: 
    <br /> a. parameter encoding.
    <br /> b. request.
    <br /> c. response.

Part 3: You are required to design a dynamic form to update user profile.

**Design :** zpl.io/bPXwZRp
>Zeplin invitation is sent to your email, please check out.

**Feature requirement:** 
You are required to create a view to allow user to edit their profile. User is allow to 
1. edit basic profile information
    <br /> a. first name
    <br /> b. last name
    <br /> c. age 
    <br />d. gender.
    
2. create buyer profile
    <br /> a. main purpose
    <br /> b. immigration status
    <br /> c. maximum budget
    <br /> d. amenities
    <br /> e. minumum sqft
    <br /> f. others requirement.
    
3. create tenant profile
    <br /> a. Move in date
    <br /> b. Nationality
    <br /> c. Immigration Status
    <br /> d. Maximum budget
    <br /> e. Amenities
    <br /> f. others requirement.
    
**Basic Requirements:**

You are required to design a solution which mobile clients are able to render the form UI with fully backend control.
Which means backend is able to change the 
1. the ordering of the row.
2. the content of the row. For example: include user's height and weight without the need of client changes.
3. the options for the row. For example: add "Employment Pass" to immigration status.
4. the API calls for the row. For example: add a location autocomplete row to enable user to fill in preffered location.

> create a mock json in mockable.io and call it using your network wrapper to get your designed form response.

Mockable.io google account login:

https://www.mockable.io/

username: ninetyninedevcandidate@gmail.com
password: AsdAsd123


> You must ensure backward campactible for your solution and the solution should be able to work in different platforms. 


**Detail Requirements:**

1. Toggle the switcher for buyer and tenant profile will control the visibility of extra rows. Refer to design.
2. Picker view with options ( None, Own Stay, Investment, Other) is shown right after the main purpose row after clicked on the row. Clicked on the row again will hide the picker.
3. Maximum budget and minumum sqft need to be formatted in the respective format: Refer to the design.
4. Number of amenities is shown after selection is done. Refer to design.
5. Height of the text view will changed accrodingly to the text being input in the text view for others requirement's input view.
6. Click on nationality's row will navigate to nationality autocomplete view. Use the wrapper you wrote in part 1 to call
    https://dw.99.co/mobile/v3/ios/tenant-profile/nationality-autocomplete?query=S to get the autocomplete result, click on the result
    will direct back to the form immediately with the nationality being filled in.
7. By clicking on done, client should be able to call the update profile endpoint to pass all the collected value for each rows to backend to finish the profile update process.


**Final Notes:** Please branch out from master and commit the codes directly into this repo. Commit frequently is always a good practice as an engineer :) Good luck !
