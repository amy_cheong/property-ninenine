//
//  MoreDetailTableViewController.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol MoreDetailTableViewControllerDelegate: class {
   func fieldDidChanged(array: [String], indexPath: IndexPath)
}

class MoreDetailTableViewController: UITableViewController, ViewModelable {
   typealias ViewModel = MoreDetailViewModel
   var viewModel: ViewModel!

   weak var delegate: MoreDetailTableViewControllerDelegate?

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SwitchDetailTableViewCell.self), for: indexPath) as? SwitchDetailTableViewCell else {
         return UITableViewCell()
      }
      cell.display(field: viewModel.field, indexPath: indexPath)
      if let item = viewModel.field.fields?[indexPath.row].id {
         cell.toggleSwitch.isOn = viewModel.selectedValues.contains(item)
      }

      cell.delegate = self
      return cell
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return viewModel.field.fields?.count ?? 0
   }

   override func viewWillDisappear(_ animated: Bool) {
      delegate?.fieldDidChanged(array: viewModel.selectedValues, indexPath: viewModel.indexPath)
      super.viewWillDisappear(animated)
   }
}

extension MoreDetailTableViewController: SwitchDetailTableViewCellDelegate {
   func toggleSwitch(status: Bool, value: String) {
      viewModel.valueToggled(status: status, value: value)
   }
}


