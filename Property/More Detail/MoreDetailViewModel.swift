//
//  MoreDetailViewModel.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

struct MoreDetailViewModel {

   var field: Field!
   var indexPath: IndexPath!
   var selectedValues: [String]

   init(field: Field, indexPath: IndexPath, selectedValues: [String]) {
      self.field = field
      self.indexPath = indexPath
      self.selectedValues = selectedValues
   }

   mutating func valueToggled(status: Bool, value: String) {
      if selectedValues.contains(value) && status == false {
         selectedValues = selectedValues.filter { $0 != value }
      } else if !selectedValues.contains(value) && status == true {
         selectedValues.append(value)
      }
   }
}
