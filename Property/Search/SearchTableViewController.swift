//
//  SearchTableViewController.swift
//  Property
//
//  Created by Amy Cheong on 22/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol SearchTableViewControllerDelegate: class {
   func didSelectList(with value: String, indexPath: IndexPath)
}

class SearchTableViewController: UITableViewController, UISearchBarDelegate, ErrorMessageDisplayable {
   typealias ViewModel = SearchViewModel
   var viewModel: ViewModel!

   @IBOutlet private weak var searchBar: UISearchBar!

   weak var delegate: SearchTableViewControllerDelegate?
   
    override func viewDidLoad() {
         super.viewDidLoad()

      viewModel.tableDataHandler = { [weak self] result in
         if let error = result.error {
            self?.presentPopup(title: error.title, message: error.message,
                              buttonTitle: "OK", onClose: {})
         }

         self?.tableView.reloadData()
      }
    }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchDetailTableViewCell.identifier, for: indexPath) as? SearchDetailTableViewCell else {
         return UITableViewCell()
      }

      let item = viewModel.nationalityListResponse.list[indexPath.row]
      cell.titleLabel.text = item
      return cell
   }

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let item = viewModel.nationalityListResponse.list[indexPath.row]
      delegate?.didSelectList(with: item, indexPath: viewModel.indexPath)
     self.navigationController?.popViewController(animated: true)
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return viewModel.nationalityListResponse.list.count
   }

   // MARK: - UISearchBarDelegate
   func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      viewModel.getNationalityList(with: searchText)
   }
}
