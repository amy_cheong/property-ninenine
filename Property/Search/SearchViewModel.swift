//
//  SearchViewModel.swift
//  Property
//
//  Created by Amy Cheong on 22/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

class SearchViewModel {

   typealias DataItem = (list: [String], error: Error?)
   private(set) var nationalityService = CoreUseCase.shared.nationalityService

   var tableDataHandler: ((DataItem) -> ())?
   private(set) var nationalityListResponse: DataItem = ([], nil) {
      didSet { tableDataHandler?(nationalityListResponse) }
   }

   var indexPath: IndexPath!

   init(indexPath: IndexPath) {
      self.indexPath = indexPath
   }

   func getNationalityList(with searchText: String){
      nationalityService.getNationalityList(keyword: searchText) { [weak self] result in
         guard let `self` = self else { return }
         switch result {
         case .success(let list):
            guard let list = list else { return }
            self.nationalityListResponse = (list.data.nationalities, nil)
         case .error(let error):
            self.nationalityListResponse = ([], error)
         }
      }
   }

}
