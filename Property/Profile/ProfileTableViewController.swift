//
//  ProfileTableViewController.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController, ViewModelable, ErrorMessageDisplayable {

   typealias ViewModel = ProfileViewModel
   var viewModel: ViewModel! = ViewModel()

   override func viewDidLoad() {
      super.viewDidLoad()
      tableView.estimatedRowHeight = 100
      tableView.rowHeight = UITableView.automaticDimension
      viewModel.tableDataHandler = { [weak self] _ in
         self?.tableView.reloadData()
      }

      viewModel.createProfileHandler = {[weak self] result in
         guard let `self` = self, let result = result else { return }
         switch result {
         case .success(let item):
            self.presentPopup(title: "Create Profile", message: item?.description ?? "",
                              buttonTitle: "OK", onClose: {})
            self.tableView.reloadData()
         case .error(let error):
            self.presentPopup(title: error.title, message: error.message,
                              buttonTitle: "OK", onClose: {})
         }
      }
   }

   //MARK: UITableViewDataSource
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let profile = viewModel.profiles[indexPath.section]
      let field = profile.fields[indexPath.row]


      switch field.fieldType {
      case .labelView:

         let cell: TextfieldTableViewCell = tableView.dequeueCell(with: indexPath)
         let defaultValue = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? String
         cell.display(field: field, indexPath: indexPath)
         cell.textfield.text = defaultValue
         cell.delegate = self
         return cell

      case .collectionButtonView:

         let cell: CollectionTableViewCell = tableView.dequeueCell(with: indexPath)
         let defaultValue = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? String ?? ""
         cell.display(field: field, indexPath: indexPath, selectedValue: defaultValue)
         cell.delegate = self
         return cell

      case .pickerView:
         let cell: PickerTableViewCell = tableView.dequeueCell(with: indexPath)
         let selectedValueID = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? String
         cell.display(field: field, indexPath: indexPath, selectedValueID: selectedValueID)
         cell.delegate = self
         return cell

      case .toggleSwitchView:

         let cell: SwitchTableViewCell = tableView.dequeueCell(with: indexPath)
         cell.display(profile: profile, indexPath: indexPath)
         switch profile.id {
         case .buyer:
            cell.toggleSwitch.isOn = viewModel.buyerEnabled
         case .tenant:
            cell.toggleSwitch.isOn = viewModel.tenantEnabled
         default: break
         }
         cell.delegate = self
         return cell

      case .detailView:
         let cell: MoreDetailTableViewCell = tableView.dequeueCell(with: indexPath)
         cell.titleLabel.text = field.displayName

         var text = ""
         if let defaultValues = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? [String],
            defaultValues.count > 0 {
            text = "\(defaultValues.count)"
         }
         cell.selectedCountLabel.text = text

         return cell

      case .listView:
         let cell: SearchTableViewCell = tableView.dequeueCell(with: indexPath)
         let selectedValue = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? String
         cell.titleLabel.text = field.displayName
         cell.selectedLabel.text = selectedValue
         return cell

      case .textView:
         let cell: TextViewTableViewCell = tableView.dequeueCell(with: indexPath)
         cell.display(indexPath: indexPath, field: field)
         let defaultValue = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? String
         cell.detailTextView.text = defaultValue
         cell.delegate = self
         return cell

      default: return UITableViewCell()
      }
   }

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let cell: PickerTableViewCell = tableView.dequeueCell(with: indexPath)
      UIView.animate(withDuration: 0.2, animations: {
         tableView.beginUpdates()
         cell.isExpand = !cell.isExpand
         tableView.endUpdates()
      })
   }

   override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
      guard let cell = tableView.cellForRow(at: indexPath) as? PickerTableViewCell
         else { return }
      UIView.animate(withDuration: 0.2, animations: {
         tableView.beginUpdates()
         cell.isExpand = false
         tableView.endUpdates()
      })
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let section = viewModel.profiles[section]
      switch section.id {
      case .basic: return section.fields.count
      case .tenant:
         let count = viewModel.tenantEnabled ? section.fields.count : 1
         return count
      case .buyer:
         let count = viewModel.buyerEnabled ? section.fields.count : 1
         return count
      }
   }

   override func numberOfSections(in tableView: UITableView) -> Int {
      return viewModel.profiles.count
   }

   override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      return viewModel.profiles[section].displayName
   }

   // MARK: - Navigation
   enum SegueIndentifier: String {
      case moreDetail
      case search
   }

   // MARK: Segues
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard let identString = segue.identifier,
         let identifier = SegueIndentifier(rawValue: identString) else {
            super.prepare(for: segue, sender: sender)
            return
      }

      guard let indexPath = tableView.indexPathForSelectedRow else { return }
      let profile = viewModel.profiles[indexPath.section]
      let field = profile.fields[indexPath.row]

      switch identifier {
      case .moreDetail:
         let array = viewModel.createProfileRequest[profile.id.rawValue]?[field.id] as? [String]

         if let destinationViewController = segue.destination as? MoreDetailTableViewController{
            destinationViewController.delegate = self
            destinationViewController.viewModel = MoreDetailViewModel(field: field,
                                                                      indexPath: indexPath,
                                                                      selectedValues: array ?? [])
         }
      case .search:

         if let destinationViewController = segue.destination as? SearchTableViewController{
            destinationViewController.delegate = self
            destinationViewController.viewModel = SearchViewModel(indexPath: indexPath)
         }
      }


   }
   @IBAction func saveProfileButtonPressed(_ sender: Any) {
      // To make sure we stop all the editing on all the fields
      view.endEditing(true)
      viewModel.createProfile()
   }
}

// MARK: - TextViewTableViewCellDelegate
extension ProfileTableViewController: TextViewTableViewCellDelegate {

   func textViewDidChange(_ textView: UITextView, indexPath: IndexPath) {

      let size = textView.bounds.size
      let newSize = textView.sizeThatFits(CGSize(width: size.width,
                                                 height: CGFloat.greatestFiniteMagnitude))

      // Resize the cell only when cell's size is changed
      if size.height != newSize.height {
         UIView.setAnimationsEnabled(false)
         tableView?.beginUpdates()
         tableView?.endUpdates()
         UIView.setAnimationsEnabled(true)

         let thisIndexPath = indexPath
         tableView?.scrollToRow(at: thisIndexPath,
                                at: .bottom, animated: false)
      }
      viewModel.updateRequest(indexPath: indexPath, newValue: textView.text ?? "")
   }
}

// MARK: - SwitchTableViewCellDelegate
extension ProfileTableViewController: SwitchTableViewCellDelegate{

   func toggleSwitch(status: Bool, profile: Profile) {
      switch profile.id {
      case .buyer:
         viewModel.buyerEnabled = status
      case .tenant:
         viewModel.tenantEnabled = status
      default: break
      }

      var indexPathArray: [IndexPath] = []
      for i in 1..<profile.fields.count {
         let indexPath = IndexPath(row: i, section: profile.id.numberValue)
         indexPathArray.append(indexPath)
      }

      tableView.beginUpdates()
      if status {
         tableView.insertRows(at: indexPathArray, with: .automatic)
      } else {
         tableView.deleteRows(at: indexPathArray, with: .fade)
      }
      tableView.endUpdates()
   }
}



// MARK: - TextfieldTableViewCellDelegate
extension ProfileTableViewController: TextfieldTableViewCellDelegate {
   func textFieldDidEndEditing(_ textField: UITextField, indexPath: IndexPath) {
      let profile = viewModel.profiles[indexPath.section]
      let field = profile.fields[indexPath.row]

      var textfieldInput = textField.text ?? ""
      if let validationType = field.validationType {
         switch validationType {
         case .price:
            textfieldInput = textfieldInput.formattedWithCommaSeparator
         case .sqft:
            textfieldInput = textfieldInput
               .replacingOccurrences(of: validationType.appendText, with: "")
               .formattedWithCommaSeparator
            textfieldInput = textfieldInput.count > 0 ? "\(textfieldInput) \(validationType.appendText)" : ""
         case .none: break
         }
         textField.placeholder = validationType.placeholder
      }
      textField.text = textfieldInput
      viewModel.updateRequest(indexPath: indexPath, newValue: textfieldInput)
   }
}

// MARK: - MoreDetailTableViewControllerDelegate
extension ProfileTableViewController: MoreDetailTableViewControllerDelegate {
   func fieldDidChanged(array: [String], indexPath: IndexPath) {
      viewModel.updateRequest(indexPath: indexPath, newValue: array)
      tableView.reloadRows(at: [indexPath], with: .automatic)
   }
}

// MARK: - PickerTableViewCellDelegate
extension ProfileTableViewController: PickerTableViewCellDelegate {
   func didSelectRow(_ value: String, indexPath: IndexPath) {
      viewModel.updateRequest(indexPath: indexPath, newValue: value)
   }
}

// MARK: - PickerTableViewCellDelegate
extension ProfileTableViewController: CollectionTableViewCellDelegate {
   func didSelectCollectionViewRow(_ value: String, indexPath: IndexPath) {
      viewModel.updateRequest(indexPath: indexPath, newValue: value)
      tableView.reloadRows(at: [indexPath], with: .automatic)
   }
}

// MARK: - PickerTableViewCellDelegate
extension ProfileTableViewController: SearchTableViewControllerDelegate {
   func didSelectList(with value: String, indexPath: IndexPath) {
      viewModel.updateRequest(indexPath: indexPath, newValue: value)
      tableView.reloadRows(at: [indexPath], with: .automatic)
   }
}

