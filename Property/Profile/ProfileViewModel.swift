//
//  ProfileViewModel.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation
import UIKit

typealias CreateProfileDataType = [String: [String: Any]]

class ProfileViewModel {
   // Data item for tableview
   private(set) var settingsService = CoreUseCase.shared.settingsService

   var tableDataHandler: (([Profile]) -> ())?
   private(set) var profiles:  [Profile] = [] {
      didSet { tableDataHandler?(profiles) }
   }

   var createProfileHandler: ((Result<CreateProfileDataType>?) -> ())?
   private(set) var createProfileResult: Result<CreateProfileDataType>? = nil {
      didSet { createProfileHandler?(createProfileResult) }
   }

   var buyerEnabled: Bool = false
   var tenantEnabled: Bool = false
   var createProfileRequest: CreateProfileDataType = [:]

   init() {
      getAllFields()
   }

   // Get all the fields from backend to setup our fields
   func getAllFields(){
      settingsService.getAllFields { result in
         switch result {
         case .success(let profileData):
            self.profiles = profileData?.data ?? []
         case .error(let error):
            debugPrint(error.localizedDescription)
         }
      }
   }

   func updateRequest(indexPath: IndexPath, newValue: Any){
      let profile = profiles[indexPath.section]
      let field = profile.fields[indexPath.row]
      let input = newValue


      // Need to construct a dynamic
      // 1. check if profile id is created
      if !createProfileRequest.keys.contains(profile.id.rawValue) {
         // 2. if not create an empty
         createProfileRequest[profile.id.rawValue] = [:]
      }

      // 3. profile should be created by now, time for fields
      createProfileRequest[profile.id.rawValue]?[field.id] = input
   }

   // Send current filled fields to server for profile creation
   func createProfile(){
      // Delete Buyer section IF request dict has disabled section values
      if createProfileRequest.keys.contains(ProfileID.buyer.rawValue) && !buyerEnabled {
         createProfileRequest.removeValue(forKey: ProfileID.buyer.rawValue)
      }

      // Delete tenant section IF request dict has disabled section values
      if createProfileRequest.keys.contains(ProfileID.tenant.rawValue) && !tenantEnabled {
         createProfileRequest.removeValue(forKey: ProfileID.tenant.rawValue)
      }

      settingsService.postCreateProfile(createProfileRequest: createProfileRequest) { [weak self] (result) in
         guard let `self` = self else { return }
         switch result {
         case .success(let item):
            guard let item = item else { return }
            // CREATEPROFILEREQUEST: Removed this if you do not want autofilled from response
            self.createProfileRequest = item
         case .error: break
         }
         self.createProfileResult = result
      }
   }


}
