//
//  NationalityService.swift
//  Property
//
//  Created by Amy Cheong on 23/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

class NationalityService {

   enum Endpoints {
      case getNationalityList(keyword: String)

      var urlString: String {
         switch self {
         case .getNationalityList(let keyword):
            return "https://dw.99.co/mobile/v3/ios/tenant-profile/nationality-autocomplete?query=\(keyword)"
         }
      }
   }

   func getNationalityList(keyword: String, completion: @escaping (Result<NationalityDataResponse>) -> Void) {
      NetworkLayer().get(urlString: Endpoints.getNationalityList(keyword: keyword).urlString,
                         completion: completion)
   }

}
