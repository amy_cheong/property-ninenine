//
//  ProfileService.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

class SettingsService {

   enum Endpoints {
      case getAllFields
      case createProfile

      var urlString: String {
         switch self {
         case .getAllFields:
            return "https://demo6978519.mockable.io/settings/get"
         case .createProfile:
            return "https://demo6978519.mockable.io/profile"
         }
      }
   }

   func getAllFields(completion: @escaping (Result<ProfileDataResponse>) -> Void) {
      NetworkLayer().get(urlString: Endpoints.getAllFields.urlString, completion: completion)
   }

   func postCreateProfile(createProfileRequest: CreateProfileDataType,
                          completion: @escaping (Result<CreateProfileDataType>) -> Void) {
      do {
         let jsonData = try JSONSerialization.data(withJSONObject: createProfileRequest,
                                                   options: .prettyPrinted)

         NetworkLayer().post(urlString: Endpoints.createProfile.urlString,
                             body: jsonData) { (result) in
               switch result {
               case .success(let data):
                  guard let data = data,
                     let decoded = try? JSONSerialization.jsonObject(with: data, options: []) as? CreateProfileDataType else {
                     completion(.error(Error.parseError))
                     return
                  }
                  completion(.success(decoded))
               case .error(let error):
                  completion(.error(error))
               }
            }

      } catch {
         completion(.error(Error.parseError))
      }
   }

}
