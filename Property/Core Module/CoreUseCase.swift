//
//  CoreUseCase.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

final class CoreUseCase {

   static let shared = CoreUseCase()

   let settingsService: SettingsService = SettingsService()
   let nationalityService: NationalityService = NationalityService()

   private init() {}
}
