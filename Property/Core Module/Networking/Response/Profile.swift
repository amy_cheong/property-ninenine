//
//  BasicProfile.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

struct ProfileDataResponse: Codable {
   var data: [Profile] = []
}


struct Profile: Codable {
   var id: ProfileID
   var displayName: String
   var fields: [Field]

   init(name: String, id: String, fields: [Field]) {
      self.displayName = name
      self.id = ProfileID(rawValue: id) ?? .basic
      self.fields = fields
   }
}

