//
//  Basic.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

struct Field: Codable {
   var displayName: String
   var id: String
   var fieldType: FieldType
   var validationType: ValidationType?
   var fields: [Field]?

   init(id: String, displayName: String, fieldType: Int, fields: [Field]?, validationType: String?) {
      self.id = id
      self.displayName = displayName
      self.fieldType = FieldType(rawValue: fieldType) ?? .none
      self.fields = fields

      if let validationType = validationType, let type = ValidationType(rawValue: validationType) {
         self.validationType = type
      } else {
         self.validationType = .none
      }
   }
}
