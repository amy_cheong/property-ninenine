//
//  ProfileID.swift
//  Property
//
//  Created by Amy Cheong on 21/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

enum ProfileID: String, Codable {
   case basic
   case buyer
   case tenant

   var numberValue: Int {
      switch self {
      case .basic: return 0
      case .buyer: return 1
      case .tenant: return 2
      }
   }
}
