//
//  Nationality.swift
//  Property
//
//  Created by Amy Cheong on 23/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

struct NationalityDataResponse: Codable {
   var data: NationalityList
}

struct NationalityList: Codable {
   var nationalities: [String]
}


