//
//  FieldType.swift
//  Property
//
//  Created by Amy Cheong on 24/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

enum FieldType: Int, Codable {
   case none = 0
   case labelView
   case collectionButtonView
   case toggleSwitchView
   case pickerView
   case detailView
   case textView
   case listView
}
