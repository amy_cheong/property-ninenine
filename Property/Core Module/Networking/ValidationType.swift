//
//  ValidationType.swift
//  Property
//
//  Created by Amy Cheong on 24/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

enum ValidationType: String, Codable {
   case none
   case sqft
   case price

   var placeholder: String {
      switch self {
      case .price, .sqft:
         return "Numbers only"
      default:
         return ""
      }
   }

   var appendText: String {
      switch self {
      case .sqft:
         return "sqft"
      default:
         return ""
      }
   }
}
