//
//  NetworkLayer.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

public enum Result<T> {
   case success(T?)
   case error(Error)
}

public enum Error: Swift.Error {
   case generic
   case parseError

   var title: String {
      switch self {
      case .generic:
         return "Oops"
      case .parseError:
         return "Error"
      }
   }

   var message: String {
      switch self {
      case .generic:
         return "Something went wrong. Please try again later"
      case .parseError:
         return "Cannot parse given object"
      }
   }
}

class NetworkLayer {
   private func isSuccessCode(_ response: URLResponse?) -> Bool {
      guard let urlResponse = response as? HTTPURLResponse else {
         return false
      }
      return urlResponse.statusCode >= 200 && urlResponse.statusCode < 300
   }


   func get<ExpectedResult: Codable>(urlString: String,
                                     completion: @escaping (Result<ExpectedResult>) -> Void) {

      func fail(error: Error) {
         DispatchQueue.main.async {
            completion(.error(error))
         }
      }

      func success(data: ExpectedResult) {
         DispatchQueue.main.async {
            completion(.success(data))
         }
      }

      guard let url = URL(string: urlString) else {
         return fail(error: Error.generic)
      }
      let request = URLRequest(url: url)
      URLSession.shared.dataTask(with: request){ (data, urlResponse, error) in
         if let error = error {
            print(error.localizedDescription)
            fail(error: Error.generic)
            return
         }

         if self.isSuccessCode(urlResponse) {
            guard let data = data else {
               print("Unable to parse the response in given type \(ExpectedResult.self)")
               return fail(error: Error.generic)
            }

            if let responseObject = try? JSONDecoder().decode(ExpectedResult.self, from: data) {
               success(data: responseObject)
               return
            }
         }
         fail(error: Error.generic)
         }.resume()
   }

   func post<ExpectedResult: Codable>(urlString: String,
                                      body: ExpectedResult,
                                      httpMethod: HTTPMethod = .post,
                                      headers: [String: String] = [:],
                                      completion: @escaping (Result<ExpectedResult>) -> Void) {

      func fail(error: Error) {
         DispatchQueue.main.async {
            completion(.error(error))
         }
      }

      func success(data: ExpectedResult) {
         DispatchQueue.main.async {
            completion(.success(data))
         }
      }

      guard let url = URL(string: urlString) else {
         return fail(error: Error.generic)
      }

      var request = URLRequest(url: url)
      request.timeoutInterval = 90
      request.httpMethod = httpMethod.rawValue
      request.allHTTPHeaderFields = headers
      request.allHTTPHeaderFields?["Content-Type"] = "application/json"
      guard let data = try? JSONEncoder().encode(body) else {
         return fail(error: Error.parseError)
      }
      request.httpBody = data
      URLSession.shared.uploadTask(with: request,
                                   from: data) { (data, urlResponse, error) in
                                    if self.isSuccessCode(urlResponse) {
                                       guard let data = data else {
                                          print("Unable to parse the response in given type \(ExpectedResult.self)")
                                          return fail(error: Error.generic)
                                       }
                                       if let responseObject = try? JSONDecoder().decode(ExpectedResult.self, from: data) {
                                          success(data: responseObject)
                                          return
                                       }
                                    }
                                    fail(error: Error.generic)
         }.resume()
   }

   func post(urlString: String, body: Data,
             httpMethod: HTTPMethod = .post,
             headers: [String: String] = [:],
             completion: @escaping (Result<Data>) -> Void) {

      func fail(error: Error) {
         DispatchQueue.main.async {
            completion(.error(error))
         }
      }

      func success(data: Data?) {
         DispatchQueue.main.async {
            completion(.success(data))
         }
      }

      guard let url = URL(string: urlString) else {
         return fail(error: Error.generic)
      }

      var request = URLRequest(url: url)
      request.timeoutInterval = 90
      request.httpMethod = httpMethod.rawValue
      request.allHTTPHeaderFields = headers
      request.allHTTPHeaderFields?["Content-Type"] = "application/json"
      request.httpBody = body
      
      URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
         if self.isSuccessCode(urlResponse) {
            success(data: data)
         } else {
            fail(error: Error.generic)
         }
      }.resume()
   }
}
