//
//  Formatter+Extension.swift
//  Property
//
//  Created by Amy Cheong on 21/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation

extension Formatter {
   static let withCommaSeparator: NumberFormatter = {
      let formatter = NumberFormatter()
      formatter.groupingSeparator = ","
      formatter.numberStyle = .decimal
      return formatter
   }()
}

extension String {
   var formattedWithCommaSeparator: String {
      let number = Double(self)
      return Formatter.withCommaSeparator.string(for: number) ?? ""
   }
}
