//
//  TextfieldTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 16/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol TextfieldTableViewCellDelegate: class {
   func textFieldDidEndEditing(_ textField: UITextField, indexPath: IndexPath)
}

class TextfieldTableViewCell: UITableViewCell {

   @IBOutlet weak var nameLabel: UILabel!
   @IBOutlet weak var textfield: UITextField!

   weak var delegate: TextfieldTableViewCellDelegate?

   var indexPath: IndexPath!
   var field: Field!

   func display(field: Field, indexPath: IndexPath){
      self.indexPath = indexPath
      self.field = field
      nameLabel.text = field.displayName
      if let validationType = self.field.validationType {
         switch validationType {
         case .price, .sqft:
            textfield.keyboardType = .decimalPad
         default:
            textfield.keyboardType = .default
         }
      }
      textfield.delegate = self
   }
   
}

extension TextfieldTableViewCell: UITextFieldDelegate {
   func textFieldDidEndEditing(_ textField: UITextField) {
      delegate?.textFieldDidEndEditing(textField, indexPath: indexPath)
   }

   func textFieldDidBeginEditing(_ textField: UITextField) {
      guard let validationType = self.field.validationType else {
         return
      }

      // Remove comma, space and text that we added in earlier
      textfield.text = textfield.text?.replacingOccurrences(of: " ", with: "")
      textfield.text = textfield.text?.replacingOccurrences(of: ",", with: "")
      textfield.text = textfield.text?.replacingOccurrences(of: validationType.appendText, with: "")
   }
}

