//
//  CollectionTableViewCollectionViewCell.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

class CollectionTableViewCollectionViewCell: UICollectionViewCell {

   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var itemView: UIView!

   func display(field: Field){
      titleLabel.text = field.displayName
      itemView.layer.cornerRadius = 4.0
      itemView.layer.borderColor = tintColor.cgColor
      itemView.layer.borderWidth = 1.0
   }

   var active: Bool = false {
      didSet {
         itemView.backgroundColor = active ? tintColor : .white
         titleLabel.textColor = active ? .white : tintColor
      }
   }

}
