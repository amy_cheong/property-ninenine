//
//  SwitchTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol SwitchTableViewCellDelegate: class {
   func toggleSwitch(status: Bool, profile: Profile)
}

class SwitchTableViewCell: UITableViewCell {

   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var toggleSwitch: UISwitch!

   weak var delegate: SwitchTableViewCellDelegate?
   var profile: Profile!

   func display(profile: Profile, indexPath: IndexPath){
      self.profile = profile
      titleLabel.text = profile.fields[indexPath.row].displayName
   }

   @IBAction func toggleSwitch(_ sender: Any) {
      delegate?.toggleSwitch(status: toggleSwitch.isOn, profile: profile)
   }
}
