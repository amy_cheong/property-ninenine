//
//  TextViewTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol TextViewTableViewCellDelegate: class {
   func textViewDidChange(_ textView: UITextView, indexPath: IndexPath)
}

class TextViewTableViewCell: UITableViewCell {

   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var detailTextView: UITextView!

   weak var delegate: TextViewTableViewCellDelegate?
   var indexPath: IndexPath!
   
   /// Custom setter so we can initialize the height of the text view
   var textString: String {
      get {
         return detailTextView?.text ?? ""
      }
      set {
         if let textView = detailTextView {
            textView.text = newValue
            textView.delegate?.textViewDidChange?(textView)
         }
      }
   }

   func display(indexPath: IndexPath, field: Field){
      titleLabel.text = field.displayName
      self.indexPath = indexPath
      detailTextView.delegate = self
   }

   override func awakeFromNib() {
      super.awakeFromNib()
      // Disable scrolling inside the text view so we enlarge to fitted size
      detailTextView?.isScrollEnabled = false
   }

   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)

      if selected {
         detailTextView?.becomeFirstResponder()
      } else {
         detailTextView?.resignFirstResponder()
      }
   }
}

extension TextViewTableViewCell: UITextViewDelegate {
   func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange,
                 replacementText text: String) -> Bool {
      //if you hit "Enter" you resign first responder
      //and don't put this character into text view text
      if text == "\n" {
         textView.resignFirstResponder()
         return false
      }
      return true
   }

   func textViewDidChange(_ textView: UITextView) {
      delegate?.textViewDidChange(textView, indexPath: indexPath)
   }
}
