//
//  SearchTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 23/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var selectedLabel: UILabel!

}

