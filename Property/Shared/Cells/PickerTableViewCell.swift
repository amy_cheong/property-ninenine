//
//  PickerTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol PickerTableViewCellDelegate: class {
   func didSelectRow(_ value: String, indexPath: IndexPath)
}

class PickerTableViewCell: UITableViewCell {
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var pickerView: UIPickerView!
   @IBOutlet weak var selectedLabel: UILabel!

   @IBOutlet weak var pickerViewHeightConstraint: NSLayoutConstraint!
   var field: Field!
   var indexPath: IndexPath!

   weak var delegate: PickerTableViewCellDelegate?

   var isExpand: Bool = false {
      didSet {
         let height: CGFloat = isExpand ? 130.0 : 0.0
         pickerView.isHidden = !isExpand
         pickerViewHeightConstraint.constant = height
      }
   }

   func display(field: Field, indexPath: IndexPath, selectedValueID: String?){
      self.field = field
      self.indexPath = indexPath
      titleLabel.text = field.displayName

      if let selectedValueID = selectedValueID, let fields = field.fields {
         let selectedValue = fields.filter({ $0.id == selectedValueID })
         selectedLabel.text = selectedValue.first?.displayName
      }
      pickerView.delegate = self
      pickerView.dataSource = self
   }
}

extension PickerTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
   }

   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
      return field?.fields?.count ?? 0
   }

   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return field?.fields?[row].displayName
   }

   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){

      guard let value = field?.fields?[row] else { return }
      selectedLabel.text = value.displayName
      delegate?.didSelectRow(value.id, indexPath: self.indexPath)
   }
}
