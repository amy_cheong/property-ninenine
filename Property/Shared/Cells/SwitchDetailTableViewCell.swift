//
//  SwitchDetailTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 19/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol SwitchDetailTableViewCellDelegate: class {
   func toggleSwitch(status: Bool, value: String)
}

class SwitchDetailTableViewCell: UITableViewCell {
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var toggleSwitch: UISwitch!

   weak var delegate: SwitchDetailTableViewCellDelegate?
   var field: Field!
   var indexPath: IndexPath!

   func display(field: Field, indexPath: IndexPath){
      self.field = field
      self.indexPath = indexPath
      titleLabel.text = field.fields?[indexPath.row].displayName
   }

   @IBAction func toggleSwitch(_ sender: Any) {
      guard let fields = field.fields else { return }
      delegate?.toggleSwitch(status: toggleSwitch.isOn, value: fields[indexPath.row].id)
   }
}
