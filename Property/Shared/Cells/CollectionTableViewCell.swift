//
//  CollectionTableViewCell.swift
//  Property
//
//  Created by Amy Cheong on 17/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import UIKit

protocol CollectionTableViewCellDelegate: class {
   func didSelectCollectionViewRow(_ value: String, indexPath: IndexPath)
}

class CollectionTableViewCell: UITableViewCell {

   @IBOutlet weak var collectionViewHeightContraint: NSLayoutConstraint!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var collectionView: UICollectionView!

   // Metric
   @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
   @IBOutlet weak var collectionViewTrailingConstraint: NSLayoutConstraint!
   @IBOutlet weak var collectionViewLeadingConstraint: NSLayoutConstraint!

   var field: Field!
   var delegate: CollectionTableViewCellDelegate?
   var indexPath: IndexPath!
   var selectedValue: String!

   func display(field: Field, indexPath: IndexPath, selectedValue: String){
      self.field = field
      titleLabel.text = field.displayName
      self.indexPath = indexPath
      self.selectedValue = selectedValue

      guard let fields = field.fields else { return }

      collectionViewHeightContraint.constant = getCollectionViewHeight(with: fields)
      collectionView.delegate = self
      collectionView.dataSource = self
      collectionView.reloadData()
   }

   private func getCollectionViewHeight(with fields: [Field]) -> CGFloat{
      let itemCountPerRow :Int = 2
      let buttonHeight: CGFloat = collectionViewFlowLayout.itemSize.height
      let gap: CGFloat = collectionViewFlowLayout.minimumInteritemSpacing
      let buttonWidth: CGFloat = collectionViewFlowLayout.itemSize.width
      let screenWidth = UIScreen.main.bounds.size.width
      let marginSides: CGFloat = collectionViewTrailingConstraint.constant + collectionViewLeadingConstraint.constant

      let divideByItemCount = (screenWidth - marginSides) > (buttonWidth * CGFloat(itemCountPerRow) + gap) ?
         CGFloat(fields.count % itemCountPerRow) + 1.0 : CGFloat(fields.count)
      let height = divideByItemCount * buttonHeight + gap
      return height
   }

}

extension CollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return field.fields?.count ?? 0
   }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      guard let cell = collectionView
         .dequeueReusableCell(withReuseIdentifier: CollectionTableViewCollectionViewCell.identifier,
                              for: indexPath) as? CollectionTableViewCollectionViewCell,
      let fields = field.fields
      else { return UICollectionViewCell() }
      let item = fields[indexPath.row]
      cell.display(field: item)
      cell.active = (item.id == selectedValue)
      return cell
   }

   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      guard let item = field.fields?[indexPath.row] else {
         return
      }
      
      delegate?.didSelectCollectionViewRow(item.id, indexPath: self.indexPath)
      selectedValue = item.id
      collectionView.reloadData()
   }
}
