//
//  SearchViewModelTests.swift
//  PropertyTests
//
//  Created by Amy Cheong on 24/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import XCTest
@testable import Property

class SearchViewModelTests: XCTestCase {
   var viewModel: SearchViewModel = SearchViewModel(indexPath: IndexPath(row: 0, section: 0))
   let expectation = XCTestExpectation()

   func testGetNationalityList() {
      let result = ["Senegal", "Serbia", "Seychelles", "Holy See"]
      viewModel.tableDataHandler = { [weak self] tableItems in
         XCTAssertEqual(result, tableItems.list)
         self?.expectation.fulfill()
      }

      viewModel.getNationalityList(with: "SE")
      wait(for: [expectation], timeout: 10.0)
   }

}

