//
//  ProfileViewModelTests.swift
//  PropertyTests
//
//  Created by Amy Cheong on 24/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import XCTest
@testable import Property

class ProfileTestableModel: ProfileViewModel {

   override var settingsService: SettingsService {
      return MockSettingsService()
   }

}

class ProfileViewModelTests: XCTestCase {
   var viewModel: ProfileTestableModel = ProfileTestableModel()
   let expectation = XCTestExpectation()

   func testGetSettings() {
      viewModel.tableDataHandler = { [weak self] tableItems in
         if tableItems.count == 3 {
            self?.expectation.fulfill()
         }
      }

      viewModel.getAllFields()
      wait(for: [expectation], timeout: 10.0)
   }

   func testCreateProfile(){
      viewModel.createProfileHandler = { [weak self] result in
         guard let `self` = self, let result = result else { return }
         switch result {
         case .success(let item):
            guard let item = item else { return }
            XCTAssertEqual(self.viewModel.createProfileRequest.keys, item.keys)
            self.expectation.fulfill()
         default: break
         }

      }
      viewModel.createProfile()
      wait(for: [expectation], timeout: 10.0)
   }

   func testUpdateRequest(){
      viewModel.getAllFields()

      // Add a new field input - Last name
      let indexPath = IndexPath(item: 1, section: 0)
      viewModel.updateRequest(indexPath: indexPath, newValue: "LastNameCheong")
      XCTAssertEqual(self.viewModel.createProfileRequest["basic"]!["lastName"] as! String, "LastNameCheong")

      // Edit a new field input - Last name
      viewModel.updateRequest(indexPath: indexPath, newValue: "NewNameCheong")
      XCTAssertEqual(self.viewModel.createProfileRequest["basic"]!["lastName"] as! String, "NewNameCheong")

   }
}

