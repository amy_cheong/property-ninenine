//
//  MockSettingsService.swift
//  PropertyTests
//
//  Created by Amy Cheong on 24/2/19.
//  Copyright © 2019 Amy Cheong. All rights reserved.
//

import Foundation
@testable import Property

class MockSettingsService: SettingsService {

   /*
    Mockable data to GET all the dynamic fields from backend http://demo6978519.mockable.io/settings/get
    */
   override func getAllFields(completion: @escaping (Result<ProfileDataResponse>) -> Void) {
      let profileDataResponse: ProfileDataResponse = readJson(with: "MockableGETSettingsJson") ?? ProfileDataResponse()
      return completion(.success(profileDataResponse))
   }


   /*
    Mockable data to POST create profile http://demo6978519.mockable.io/profile
    */
   override func postCreateProfile(createProfileRequest: CreateProfileDataType, completion: @escaping (Result<CreateProfileDataType>) -> Void) {
      let createProfileResponse: CreateProfileDataType = readJsonData(with: "MockablePOSTCreateProfileJson") ?? [:]
      return completion(.success(createProfileResponse))
   }

   private func readJson<T: Codable>(with filename: String) -> T? {
      do {
         if let file = Bundle.main.url(forResource: filename, withExtension: "json") {
            let data = try Data(contentsOf: file)

            if let responseObject = try? JSONDecoder().decode(T.self, from: data) {
               return responseObject
            }
         } else {
            print("no file")
         }

      } catch {
         print(error.localizedDescription)
      }
      return nil
   }

   private func readJsonData(with filename: String) -> CreateProfileDataType? {
      do {
         if let file = Bundle.main.url(forResource: filename, withExtension: "json") {
            let data = try Data(contentsOf: file)

            guard let decoded = try? JSONSerialization.jsonObject(with: data, options: []) as? CreateProfileDataType else {
                  return nil
            }
            return decoded
         } else {
            print("no file")
         }
      } catch {
         print(error.localizedDescription)
      }
      return nil
   }
}
